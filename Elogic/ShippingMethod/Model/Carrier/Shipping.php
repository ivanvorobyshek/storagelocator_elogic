<?php

namespace Elogic\ShippingMethod\Model\Carrier;

use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Psr\Log\LoggerInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Checkout\Model\Session;
use Magento\Quote\Model\Quote\Address\RateResult\Error;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Shipping extends AbstractCarrier implements CarrierInterface
{

    /**
     * @var string
     */
    protected $_code = 'elogicshipping';

    /**
     * @var Session
     */
    private Session $session;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $productCollectionFactory;

    /**
     * @var ResultFactory
     */
    protected ResultFactory $rateResultFactory;

    /**
     * @var MethodFactory
     */
    protected MethodFactory $rateMethodFactory;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $rateResultFactory
     * @param MethodFactory $rateMethodFactory
     * @param Session $session
     * @param CollectionFactory $productCollectionFactory
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory         $rateErrorFactory,
        LoggerInterface      $logger,
        ResultFactory        $rateResultFactory,
        MethodFactory        $rateMethodFactory,
        Session $session,
        CollectionFactory $productCollectionFactory,
        array                $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->session = $session;
        $this->productCollectionFactory = $productCollectionFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * Get Allowed methods
     *
     * @return array
     */
    public function getAllowedMethods():array
    {
        return [$this->_code => $this->getConfigData('type')];
    }

    /**
     * Calculate Shipping Cost
     *
     * @return float
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function calculateShippingCost(): float
    {
        $quote = $this->session->getQuote();
        $type = $this->getConfigData('type');
        $feeType = $this->getConfigData('handling_type');
        $handlingFee = $this->getConfigData('handling_fee');
        if ($type === "PerOrder") {
            //PerOrder
            if ($feeType === "F") {
                //Fixed
                $shippingPrice = $handlingFee;
            } else {
                //Percent
                $shippingPrice = $quote->getBaseSubtotal() * $handlingFee / 100.0;
            }
        } else {
            //PerItem
            $shippingPrice = 0;
            $sku = [];
            $items = $quote->getItems();
            if (isset($items)) {
                foreach ($items as $item) {
                    $sku[] = $item->getSku();
                }
                $productCollection = $this->productCollectionFactory->create();
                $its = $productCollection
                    ->addAttributeToSelect('product_specific_fee')
                    ->addAttributeToFilter('sku', $sku)
                    ->getItems();
                $productFeeArr = [];
                if (isset($its)) {
                    foreach ($its as $it) {
                        $productFeeArr[$it->getData('sku')] = $it->getData('product_specific_fee');
                    }
                }
                if ($feeType === "F") {
                    //Fixed
                    foreach ($items as $item) {
                        $productFee = $productFeeArr[$item->getSku()] ?: $handlingFee;
                        $qty = $item->getQty();
                        $shippingPrice = $shippingPrice + $productFee * $qty;
                    }
                } else {
                    //Percent
                    foreach ($items as $item) {
                        $productFee = $productFeeArr[$item->getSku()] ?: $handlingFee;
                        $qty = $item->getQty();
                        $price = $item->getPrice();
                        $shippingPrice = $shippingPrice + $productFee * $qty * $price / 100.0;
                    }
                }
            }
        }
        return $shippingPrice;
    }

    /**
     * Collect Rates
     *
     * @param RateRequest $request
     * @return bool|DataObject|null
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }
        $quote = $this->session->getQuote();
        $orderCost = $quote->getBaseSubtotal();
        $minOrderAmount = $this->getConfigData('minimum_order_amount');
        //if min_order_amount is enabled and higher than purchase
        if (($minOrderAmount !== null) && ($minOrderAmount >= $orderCost)) {
            $error = $this->_rateErrorFactory->create();
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage(
                __('Sorry, but you can\'t choose this shipping method because
                    minimum order cost is higher than cost of your purchase.')
            );
            return $error;
        }
        $result = $this->rateResultFactory->create();
        $method = $this->rateMethodFactory->create();

        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setMethod($this->getConfigData('name'));
        $method->setMethodTitle($this->getConfigData('name'));

        $shippingPrice = $this->calculateShippingCost();
        $method->setPrice($shippingPrice);
        $method->setCost($shippingPrice);

        $result->append($method);

        return $result;
    }
}

<?php

namespace Elogic\ShippingMethod\Model\Config\Source;

class Type implements \Magento\Framework\Data\OptionSourceInterface
{

    /**
     * Options for Type
     *
     * @return array[]
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'PerOrder', 'label' => __('Per Order')],
            ['value' => 'PerItem', 'label' => __('Per Item')]
        ];
    }
}

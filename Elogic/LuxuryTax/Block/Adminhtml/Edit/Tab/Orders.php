<?php

namespace Elogic\LuxuryTax\Block\Adminhtml\Edit\Tab;

use Magento\Customer\Block\Adminhtml\Edit\Tab\Orders as BaseOrders;

class Orders extends BaseOrders
{
    /**
     * Set Collection
     *
     * @param $collection
     * @return void
     */
    public function setCollection($collection)
    {
        $collection->addFieldToSelect('luxury_tax_fee');
        parent::setCollection($collection);
    }

    /**
     * Prepare Columns
     *
     * @return $this|Orders|BaseOrders
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        $this->addColumn(
            'luxury_tax_fee',
            [
                'header' => __('Luxury Tax Fee'),
                'width' => '100', 'index' => 'luxury_tax_fee',
                'type' => 'currency',
                'currency' => 'order_currency_code',
            ]
        );
        $this->sortColumnsByOrder();
        return $this;
    }
}

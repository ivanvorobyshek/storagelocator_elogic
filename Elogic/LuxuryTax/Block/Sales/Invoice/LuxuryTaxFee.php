<?php

namespace Elogic\LuxuryTax\Block\Sales\Invoice;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class LuxuryTaxFee extends Template
{
    /**
     * Construct
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        array            $data = []
    ) {
        parent::__construct($context, $data);
        $this->setInitialFields();
    }

    /**
     * Set Initial Fields
     *
     * @return void
     */
    public function setInitialFields()
    {
        if (!$this->getLabel()) {
            $this->setLabel(__('Luxury Tax Fee'));
        }
    }

    /**
     * Init Totals
     *
     * @return $this
     */
    public function initTotals()
    {
        $order = $this->getOrder();

        $this->getParentBlock()->addTotal(
            new \Magento\Framework\DataObject(
                [
                    'code' => 'luxurytaxfee',
                    'strong' => $this->getStrong(),
                    'value' => $this->getOrder()->getLuxuryTaxFee(),
                    'base_value' => $this->getOrder()->getLuxuryTaxFee(),
                    'label' => __($this->getLabel()),
                ]
            ),
            $this->getAfter()
        );
        return $this;
    }

    /**
     * Get Order
     *
     * @return mixed
     */
    public function getOrder()
    {
        return $this->getParentBlock()->getOrder();
    }

    /**
     * Get Source
     *
     * @return mixed
     */
    public function getSource()
    {
        return $this->getParentBlock()->getSource();
    }
}

<?php

namespace Elogic\LuxuryTax\Block\Sales\Order;

use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\Order;

class LuxuryTaxFee extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Order
     */
    protected $_order;
    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_source;

    /**
     * Construct
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array                                            $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * Get Source
     *
     * @return DataObject
     */
    public function getSource()
    {
        return $this->_source;
    }

    /**
     * Display Full Summary
     *
     * @return true
     */
    public function displayFullSummary()
    {
        return true;
    }

    /**
     * Init Totals
     *
     * @return $this
     */
    public function initTotals()
    {
        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();
        $this->_source = $parent->getSource();
        $title = 'Luxury Tax Fee';
        $store = $this->getStore();
        if ($this->_order->getLuxuryTaxFee() != 0) {
            $customAmount = new \Magento\Framework\DataObject(
                [
                    'code' => 'luxurytaxfee',
                    'strong' => false,
                    'value' => $this->_order->getLuxuryTaxFee(),
                    'label' => __($title),
                ]
            );
            $parent->addTotal($customAmount, 'luxurytaxfee');
        }
        return $this;
    }

    /**
     * Get order store object
     *
     * @return \Magento\Store\Model\Store
     */
    public function getStore()
    {
        return $this->_order->getStore();
    }

    /**
     * Get Order
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * Get Label Properties
     *
     * @return array
     */
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }

    /**
     * Get Value Properties
     *
     * @return array
     */
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }
}

<?php

namespace Elogic\LuxuryTax\Api\Data;

interface LuxuryTaxInterface
{
    public const LUXURY_TAX_ID = 'entity_id';

    public const LUXURY_TAX_STATUS = 'status';

    public const LUXURY_TAX_NAME = 'name';

    public const LUXURY_TAX_DESCRIPTION = 'description';

    public const LUXURY_TAX_CUSTOMER_GROUP = 'customer_group';

    public const LUXURY_TAX_CONDITION_AMOUNT = 'condition_amount';

    public const LUXURY_TAX_TAX_RATE = 'tax_rate';

    /**
     * Get Entity ID
     *
     * @return string
     */
    public function getEntityId(): string;

    /**
     * Set Entity ID
     *
     * @param $id
     * @return LuxuryTaxInterface
     */
    public function setEntityId($id): LuxuryTaxInterface;

    /**
     * Get Status
     *
     * @return string
     */
    public function getStatus(): string;

    /**
     * Set Status
     *
     * @param $status
     * @return LuxuryTaxInterface
     */
    public function setStatus($status): LuxuryTaxInterface;

    /**
     * Get Name
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Set Name
     *
     * @param string $name
     * @return LuxuryTaxInterface
     */
    public function setName(string $name): LuxuryTaxInterface;

    /**
     * Get Description
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Set Description
     *
     * @param string $description
     * @return LuxuryTaxInterface
     */
    public function setDescription(string $description): LuxuryTaxInterface;

    /**
     * Get Customer Group
     *
     * @return string
     */
    public function getCustomerGroup(): string;

    /**
     * Set Customer Group
     *
     * @param string $customerGroup
     * @return LuxuryTaxInterface
     */
    public function setCustomerGroup(string $customerGroup): LuxuryTaxInterface;

    /**
     * Get Condition Amount
     *
     * @return string
     */
    public function getConditionAmount(): string;

    /**
     * Set Condition Amount
     *
     * @param string $conditionAmount
     * @return LuxuryTaxInterface
     */
    public function setConditionAmount(string $conditionAmount): LuxuryTaxInterface;

    /**
     * Get Tax Rate
     *
     * @return string
     */
    public function getTaxRate(): string;

    /**
     * Set Tax Rate
     *
     * @param string $taxRate
     * @return LuxuryTaxInterface
     */
    public function setTaxRate(string $taxRate): LuxuryTaxInterface;
}

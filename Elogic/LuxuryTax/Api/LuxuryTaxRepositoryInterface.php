<?php

namespace Elogic\LuxuryTax\Api;

use Elogic\LuxuryTax\Api\Data\LuxuryTaxInterface;

interface LuxuryTaxRepositoryInterface
{

    /**
     * Get Luxury Tax
     *
     * @param int $id
     * @return LuxuryTaxInterface
     */
    public function get(int $id): LuxuryTaxInterface;

    /**
     * Get List Of Luxury Taxes
     *
     * @return array
     */
    public function getList(): array;

    /**
     * Save Luxury Tax
     *
     * @param LuxuryTaxInterface $luxuryTax
     * @return bool
     */
    public function save(LuxuryTaxInterface $luxuryTax): bool;

    /**
     * Delete Luxury Tax
     *
     * @param LuxuryTaxInterface $luxuryTax
     * @return bool
     */
    public function delete(LuxuryTaxInterface $luxuryTax): bool;

    /**
     * Delete Luxury Tax by ID
     *
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id): bool;
}

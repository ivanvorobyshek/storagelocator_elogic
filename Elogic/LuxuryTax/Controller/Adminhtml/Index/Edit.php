<?php

namespace Elogic\LuxuryTax\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Exception\NoSuchEntityException;

class Edit extends Action implements HttpGetActionInterface
{

    /**
     * Authorization level of a basic admin session
     */
    public const ADMIN_RESOURCE = 'Elogic_LuxuryTax::luxurytax';

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     * @throws NotFoundException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $result->getConfig()->getTitle()->set('Edit Tax Information');
        } catch (NoSuchEntityException $e) {
            $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $result->setPath('*/*/index');
            $this->messageManager->addErrorMessage(
                __('Luxury Tax with such ID = "%value" does not exist.', ['value' => $id])
            );
        }
        return $result;
    }
}

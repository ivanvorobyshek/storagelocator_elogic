<?php

namespace Elogic\LuxuryTax\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Elogic\LuxuryTax\Model\LuxuryTaxFactory;
use Elogic\LuxuryTax\Model\ResourceModel\LuxuryTax as LuxuryTaxResource;
use Elogic\LuxuryTax\Api\LuxuryTaxRepositoryInterface;
use Magento\Framework\Controller\ResultFactory;

class Save extends Action
{

    public const ADMIN_RESOURCE = 'Elogic_LuxuryTax::luxurytax';
    /**
     * @var LuxuryTaxFactory
     */
    private LuxuryTaxFactory $luxuryTaxFactory;

    /**
     * @var LuxuryTaxResource
     */
    private LuxuryTaxResource $luxuryTaxResource;

    /**
     * @var LuxuryTaxRepositoryInterface
     */
    private LuxuryTaxRepositoryInterface $luxuryTaxRepository;

    /**
     * @param Context $context
     * @param LuxuryTaxFactory $luxuryTaxFactory
     * @param LuxuryTaxResource $luxuryTaxResource
     * @param LuxuryTaxRepositoryInterface $luxuryTaxRepository
     */
    public function __construct(
        Context $context,
        LuxuryTaxFactory $luxuryTaxFactory,
        LuxuryTaxResource $luxuryTaxResource,
        LuxuryTaxRepositoryInterface $luxuryTaxRepository,
    ) {
        $this->luxuryTaxFactory = $luxuryTaxFactory;
        $this->luxuryTaxResource = $luxuryTaxResource;
        $this->luxuryTaxRepository = $luxuryTaxRepository;
        parent::__construct($context);
    }

    /**
     * Save Luxury Tax
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $params = $data['general'];

        if (!$params) {
            $this->messageManager->addErrorMessage(__('Could not save luxury tax data.'));
        }
        if (isset($params['entity_id'])) {
            $id = (int)$params['entity_id'];
            $luxuryTax = $this->luxuryTaxFactory->create();
            $this->luxuryTaxResource->load($luxuryTax, $id);
        } else {
            $luxuryTax = $this->luxuryTaxFactory->create();
        }
        try {
            $luxuryTax->setStatus($params['status']);
            $luxuryTax->setName($params['name']);
            $luxuryTax->setDescription($params['description']);
            $luxuryTax->setCustomerGroup($params['customer_group']);
            $luxuryTax->setConditionAmount($params['condition_amount']);
            $luxuryTax->setTaxRate($params['tax_rate']);
            $this->luxuryTaxRepository->save($luxuryTax);
            $this->messageManager->addSuccessMessage(__('Luxury Tax data has been successfully saved.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $result->setPath('*/*/index');
        return $result;
    }
}

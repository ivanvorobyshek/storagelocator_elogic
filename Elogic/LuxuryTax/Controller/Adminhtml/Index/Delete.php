<?php

namespace Elogic\LuxuryTax\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Elogic\LuxuryTax\Model\LuxuryTaxFactory;
use Elogic\LuxuryTax\Model\ResourceModel\LuxuryTax as LuxuryTaxResource;
use Magento\Framework\Controller\ResultInterface;
use Elogic\LuxuryTax\Api\LuxuryTaxRepositoryInterface;

class Delete extends Action
{

    public const ADMIN_RESOURCE = 'Elogic_LuxuryTax::luxurytax';

    /**
     * @var LuxuryTaxFactory
     */
    private LuxuryTaxFactory $luxuryTaxFactory;

    /**
     * @var LuxuryTaxResource
     */
    private LuxuryTaxResource $luxuryTaxResource;

    /**
     * @var LuxuryTaxRepositoryInterface
     */
    private LuxuryTaxRepositoryInterface $luxuryTaxRepository;

    /**
     * @param Context $context
     * @param LuxuryTaxFactory $luxuryTaxFactory
     * @param LuxuryTaxResource $luxuryTaxResource
     * @param LuxuryTaxRepositoryInterface $luxuryTaxRepository
     */
    public function __construct(
        Context $context,
        LuxuryTaxFactory $luxuryTaxFactory,
        LuxuryTaxResource $luxuryTaxResource,
        LuxuryTaxRepositoryInterface $luxuryTaxRepository
    ) {
        $this->luxuryTaxFactory = $luxuryTaxFactory;
        $this->luxuryTaxResource = $luxuryTaxResource;
        $this->luxuryTaxRepository = $luxuryTaxRepository;
        parent::__construct($context);
    }

    /**
     * Delete Luxury Tax
     *
     * @return ResponseInterface|Redirect|(Redirect&ResultInterface)|ResultInterface
     */
    public function execute()
    {
        $id = (int)$this->getRequest()->getParam('id');
        if (!$id) {
            $this->messageManager->addErrorMessage(__('There is no such Luxury Tax to delete.'));
        } else {
            try {
                $luxuryTax = $this->luxuryTaxRepository->get($id);
                $this->luxuryTaxRepository->delete($luxuryTax);
                $this->messageManager->addSuccessMessage(__('Luxury Tax data has been successfully deleted.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('Something is wrong! Can\'t delete data from DB!'));
            }
        }
        $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $result->setPath('*/*/index');
        return $result;
    }
}

define(
    [
        'Elogic_LuxuryTax/js/view/checkout/summary/luxurytaxfee'
    ],
    function (Component) {
        'use strict';

        return Component.extend({
            /**
             * @override
             * use to define amount is display setting
             */
            isDisplayed: function () {
                return true;
            }
        });
    }
);

define([
    'underscore',
    'Magento_Ui/js/grid/columns/select'
], function (_, Column) {
    'use strict';

    return Column.extend({
        defaults: {
            bodyTmpl: 'Elogic_LuxuryTax/ui/grid/cells/text'
        },
        getLuxuryTaxColor: function (row) {
            let luxuryTax = row.luxury_tax_fee.replace(/[\s,%$]/g, '');
            if (parseFloat(luxuryTax) < 100) {
                return '';
            }
            if (parseFloat(luxuryTax)  < 1000) {
                return '#FFFF00';
            }
            return '#0F0';
        },
        getLuxuryTaxValue: function (row) {
            return row.luxury_tax_fee;
        }
    });
});

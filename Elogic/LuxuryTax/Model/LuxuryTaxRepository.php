<?php

namespace Elogic\LuxuryTax\Model;

use Elogic\LuxuryTax\Api\Data\LuxuryTaxInterface;
use Elogic\LuxuryTax\Api\LuxuryTaxRepositoryInterface;
use Elogic\LuxuryTax\Model\ResourceModel\LuxuryTax\CollectionFactory;
use Elogic\LuxuryTax\Model\LuxuryTaxFactory;
use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Elogic\LuxuryTax\Model\ResourceModel\LuxuryTax as LuxuryTaxResource;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InputMismatchException;

class LuxuryTaxRepository implements LuxuryTaxRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var LuxuryTaxFactory
     */
    private LuxuryTaxFactory $luxuryTaxFactory;

    /**
     * @var LuxuryTaxResource
     */
    private LuxuryTaxResource $luxuryTaxResource;

    /**
     * @var CustomerCollectionFactory
     */
    private CustomerCollectionFactory $customerCollectionFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    private CustomerRepositoryInterface $customerRepository;

    /**
     * @param CollectionFactory $collectionFactory
     * @param LuxuryTaxFactory $luxuryTaxFactory
     * @param LuxuryTaxResource $luxuryTaxResource
     * @param CustomerCollectionFactory $customerCollectionFactory
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        LuxuryTaxFactory $luxuryTaxFactory,
        LuxuryTaxResource $luxuryTaxResource,
        CustomerCollectionFactory $customerCollectionFactory,
        CustomerRepositoryInterface $customerRepository,
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->luxuryTaxFactory = $luxuryTaxFactory;
        $this->luxuryTaxResource = $luxuryTaxResource;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->customerRepository = $customerRepository;
    }

    /**
     * Get List
     *
     * @return array
     */
    public function getList(): array
    {
        return $this->collectionFactory->create()->getItems();
    }

    /**
     * Get Luxury Tax
     *
     * @param int $id
     * @return LuxuryTaxInterface
     * @throws \Exception
     */
    public function get(int $id): LuxuryTaxInterface
    {
        $luxuryTax = $this->luxuryTaxFactory->create();
        $this->luxuryTaxResource->load($luxuryTax, $id);
        if (!$luxuryTax->getId()) {
            throw new \Exception(__('Requested Luxury Tax does not exist'));
        }
        return $luxuryTax;
    }

    /**
     * Save Luxury Tax
     *
     * @param LuxuryTaxInterface $luxuryTax
     * @return bool
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws InputMismatchException
     */
    public function save(LuxuryTaxInterface $luxuryTax): bool
    {
        $luxuryTaxId = $luxuryTax->getdata('entity_id');
        if ($luxuryTaxId === null) {
            $prevLuxuryTax = null;
        } else {
            $prevLuxuryTax = $this->get($luxuryTax->getEntityId());
        }

        try {
            $this->luxuryTaxResource->save($luxuryTax);
        } catch (\Exception $exception) {
            throw new \Exception(__('Unable to save Luxury Tax #%1', $luxuryTax->getId()));
        }

        if (($prevLuxuryTax === null) || //new luxury tax
            ($prevLuxuryTax->getCustomerGroup() !== $luxuryTax->getCustomerGroup()) || //changed customer group
            ($prevLuxuryTax->getTaxRate() !== $luxuryTax->getTaxRate()) //changed luxury tax rate
        ) {
            $customerGroupId = $luxuryTax->getCustomerGroup();
            $customerTaxRate = 'Name: '. $luxuryTax->getData('name') . ', Rate: '. $luxuryTax->getData('tax_rate')
                . ', Description: ' . $luxuryTax->getData('description');

            //we need to load customers with such customer's group and to change them all tax rate
            $customers = $this->getCustomersByGroup($customerGroupId);
            foreach ($customers as $cust) {
                $customer = $this->customerRepository->getById($cust->getEntityId());
                $customer->setCustomAttribute('luxury_tax_customer_attribute', $customerTaxRate);
                $this->customerRepository->save($customer);
            }
        }
        // set 0 in customer tax rate for previous customer group
        if (($prevLuxuryTax !== null) && ($prevLuxuryTax->getCustomerGroup() !== $luxuryTax->getCustomerGroup())) {
            $customerGroupId = $prevLuxuryTax->getCustomerGroup();
            $prLuxuryTax = $this->luxuryTaxFactory->create();
            $this->luxuryTaxResource->load($prLuxuryTax, $customerGroupId, 'customer_group');
            if ($prLuxuryTax->getData('tax_rate') === null) {
                $customerTaxRate = " ";
            } else {
                $customerTaxRate = 'Name: '. $prLuxuryTax->getData('name') . ', Rate: '.
                    $prLuxuryTax->getData('tax_rate') . ', Description: ' . $prLuxuryTax->getData('description');
            }
            $customers = $this->getCustomersByGroup($customerGroupId);
            foreach ($customers as $cust) {
                $customer = $this->customerRepository->getById($cust->getEntityId());
                $customer->setCustomAttribute('luxury_tax_customer_attribute', $customerTaxRate);
                $this->customerRepository->save($customer);
            }
        }
        return true;
    }

    /**
     * Get Customer By Group
     *
     * @param $customerId
     * @return Collection
     */
    public function getCustomersByGroup($customerId)
    {
        $collection = $this->customerCollectionFactory->create();
        $collection->addFieldToSelect(['entity_id', 'group_id']);
        $collection->addFieldToFilter('group_id', ['eq' => $customerId]);
        return $collection->load();
    }

    /**
     * Delete Luxury Tax
     *
     * @param LuxuryTaxInterface $luxuryTax
     * @return bool
     * @throws InputException
     * @throws InputMismatchException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function delete(LuxuryTaxInterface $luxuryTax): bool
    {
        try {
            $this->luxuryTaxResource->delete($luxuryTax);
        } catch (\Exception $e) {
            throw new \Exception(__('Unable to remove Luxury Tax #%1', $luxuryTax->getId()));
        }
        $customerGroupId = $luxuryTax->getCustomerGroup();
        $customers = $this->getCustomersByGroup($customerGroupId);
        $customerTaxRate = " ";
        foreach ($customers as $cust) {
            $customer = $this->customerRepository->getById($cust->getEntityId());
            $customer->setCustomAttribute('luxury_tax_customer_attribute', $customerTaxRate);
            $this->customerRepository->save($customer);
        }
        return true;
    }

    /**
     * Delete Luxury Tax By ID
     *
     * @param int $id
     * @return bool
     * @throws InputException
     * @throws InputMismatchException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $id): bool
    {
        return $this->delete($this->get($id));
    }
}

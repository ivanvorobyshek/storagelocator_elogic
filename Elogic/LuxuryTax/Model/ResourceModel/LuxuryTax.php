<?php

namespace Elogic\LuxuryTax\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class LuxuryTax extends AbstractDb
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'luxury_tax_db_resource_model';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('luxury_tax_db', 'entity_id');
        $this->_useIsObjectNew = true;
    }
}

<?php

namespace Elogic\LuxuryTax\Model\ResourceModel\LuxuryTax;

use Elogic\LuxuryTax\Model\LuxuryTax as Model;
use Elogic\LuxuryTax\Model\ResourceModel\LuxuryTax as ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'luxury_tax_db_collection';

    /**
     * Initialize collection model.
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}

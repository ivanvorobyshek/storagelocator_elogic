<?php

namespace Elogic\LuxuryTax\Model;

use Elogic\LuxuryTax\Model\ResourceModel\LuxuryTax as ResourceModel;
use Magento\Framework\Model\AbstractModel;
use Elogic\LuxuryTax\Api\Data\LuxuryTaxInterface;

class LuxuryTax extends AbstractModel implements LuxuryTaxInterface
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'luxury_tax_db_model';

    /**
     * Initialize magento model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * Get Entity ID
     *
     * @return string
     */
    public function getEntityId(): string
    {
        return $this->getData(self::LUXURY_TAX_ID);
    }

    /**
     * Set Entity ID
     *
     * @param $id
     * @return LuxuryTaxInterface
     */
    public function setEntityId($id): LuxuryTaxInterface
    {
        return $this->setData(self::LUXURY_TAX_ID, (int) $id);
    }

    /**
     * Get Status
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->getData(self::LUXURY_TAX_STATUS);
    }

    /**
     * Set Status
     *
     * @param $status
     * @return LuxuryTaxInterface
     */
    public function setStatus($status): LuxuryTaxInterface
    {
        return $this->setData(self::LUXURY_TAX_STATUS, $status);
    }

    /**
     * Get Name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->getData(self::LUXURY_TAX_NAME);
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return LuxuryTaxInterface
     */
    public function setName(string $name): LuxuryTaxInterface
    {
        return $this->setData(self::LUXURY_TAX_NAME, $name);
    }

    /**
     * Get Description
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->getData(self::LUXURY_TAX_DESCRIPTION);
    }

    /**
     * Set Description
     *
     * @param string $description
     * @return LuxuryTaxInterface
     */
    public function setDescription(string $description): LuxuryTaxInterface
    {
        return $this->setData(self::LUXURY_TAX_DESCRIPTION, $description);
    }

    /**
     * Get Customer Group
     *
     * @return string
     */
    public function getCustomerGroup(): string
    {
        return $this->getData(self::LUXURY_TAX_CUSTOMER_GROUP);
    }

    /**
     * Set Customer Group
     *
     * @param string $customerGroup
     * @return LuxuryTaxInterface
     */
    public function setCustomerGroup(string $customerGroup): LuxuryTaxInterface
    {
        return $this->setData(self::LUXURY_TAX_CUSTOMER_GROUP, $customerGroup);
    }

    /**
     * Get Condition Amount
     *
     * @return string
     */
    public function getConditionAmount(): string
    {
        return $this->getData(self::LUXURY_TAX_CONDITION_AMOUNT);
    }

    /**
     * Set Condition Amount
     *
     * @param string $conditionAmount
     * @return LuxuryTaxInterface
     */
    public function setConditionAmount(string $conditionAmount): LuxuryTaxInterface
    {
        return $this->setData(self::LUXURY_TAX_CONDITION_AMOUNT, $conditionAmount);
    }

    /**
     * Get Tax Rate
     *
     * @return string
     */
    public function getTaxRate(): string
    {
        return $this->getData(self::LUXURY_TAX_TAX_RATE);
    }

    /**
     * Set Tax Rate
     *
     * @param string $taxRate
     * @return LuxuryTaxInterface
     */
    public function setTaxRate(string $taxRate): LuxuryTaxInterface
    {
        return $this->setData(self::LUXURY_TAX_TAX_RATE, $taxRate);
    }
}

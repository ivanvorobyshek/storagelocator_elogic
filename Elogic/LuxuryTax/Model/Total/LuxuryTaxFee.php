<?php

namespace Elogic\LuxuryTax\Model\Total;

use Elogic\LuxuryTax\Model\ResourceModel\LuxuryTax as LuxuryTaxResource;
use Elogic\LuxuryTax\Model\LuxuryTaxFactory;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;
use Magento\Quote\Model\QuoteValidator;

class LuxuryTaxFee extends AbstractTotal
{

    /**
     * @var LuxuryTaxResource
     */
    private LuxuryTaxResource $luxuryTaxResource;

    /**
     * @var LuxuryTaxFactory
     */
    private LuxuryTaxFactory $luxuryTaxFactory;

    /**
     * @var QuoteValidator|null
     */
    protected $quoteValidator = null;

    /**
     * Construct
     *
     * @param QuoteValidator $quoteValidator
     * @param LuxuryTaxResource $luxuryTaxResource
     * @param LuxuryTaxFactory $luxuryTaxFactory
     */
    public function __construct(
        \Magento\Quote\Model\QuoteValidator $quoteValidator,
        LuxuryTaxResource $luxuryTaxResource,
        LuxuryTaxFactory $luxuryTaxFactory
    ) {
        $this->quoteValidator = $quoteValidator;
        $this->luxuryTaxResource = $luxuryTaxResource;
        $this->luxuryTaxFactory = $luxuryTaxFactory;
    }

    /**
     * Collect
     *
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     * @return $this|LuxuryTaxFee
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);
        $customerGroupId = $quote->getCustomerGroupId();
        $luxuryTax = $this->luxuryTaxFactory->create();
        $this->luxuryTaxResource->load($luxuryTax, $customerGroupId, 'customer_group');
        $luxuryTaxCondition = $luxuryTax->getData('condition_amount');
        if ($luxuryTaxCondition == 1) {
            $luxtaxfee = $luxuryTax->getData('tax_rate');
        } else {
            $luxtaxfee = 0.0;
        }
        $total->setTotalAmount('luxurytaxfee', $luxtaxfee);
        $total->setBaseTotalAmount('luxurytaxfee', $luxtaxfee);
        $total->setLuxurytaxfee($luxtaxfee);
        $total->setBaseLuxurytaxfee($luxtaxfee);
        $total->setGrandTotal($total->getGrandTotal());
        $total->setBaseGrandTotal($total->getBaseGrandTotal());
        return $this;
    }

    /**
     * Clear Values
     *
     * @param Address\Total $total
     * @return void
     */
    protected function clearValues(Address\Total $total)
    {
        $total->setTotalAmount('subtotal', 0);
        $total->setBaseTotalAmount('subtotal', 0);
        $total->setTotalAmount('tax', 0);
        $total->setBaseTotalAmount('tax', 0);
        $total->setTotalAmount('discount_tax_compensation', 0);
        $total->setBaseTotalAmount('discount_tax_compensation', 0);
        $total->setTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setBaseTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setSubtotalInclTax(0);
        $total->setBaseSubtotalInclTax(0);
    }

    /**
     * Assign subtotal amount and label to address object
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param Address\Total $total
     * @return array
     */
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        $customerGroupId = $quote->getCustomerGroupId();
        $luxuryTax = $this->luxuryTaxFactory->create();
        $this->luxuryTaxResource->load($luxuryTax, $customerGroupId, 'customer_group');
        $luxuryTaxCondition = $luxuryTax->getData('condition_amount');
        if ($luxuryTaxCondition == 1) {
            $luxtaxfee = $luxuryTax->getData('tax_rate');
        } else {
            $luxtaxfee = 0.0;
        }
        return [
            'code' => 'luxurytaxfee',
            'title' => 'Luxury Tax Fee',
            'value' => $luxtaxfee
        ];
    }

    /**
     * Get Subtotal label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getLabel()
    {
        return __('Luxury Tax Fee');
    }
}

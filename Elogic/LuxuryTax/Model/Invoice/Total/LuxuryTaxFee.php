<?php

namespace Elogic\LuxuryTax\Model\Invoice\Total;

use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;

class LuxuryTaxFee extends AbstractTotal
{

    /**
     * Collect
     *
     * @param Invoice $invoice
     * @return $this|LuxuryTaxFee
     */
    public function collect(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        $invoice->setLuxuryTaxFee(0);
        $invoice->setBaseLuxuryTaxFee(0);

        $amount = $invoice->getOrder()->getLuxuryTaxFee();
        $invoice->setLuxuryTaxFee($amount);
        $amount = $invoice->getOrder()->getLuxuryTaxFee();
        $invoice->setBaseLuxuryTaxFee($amount);

        $invoice->setGrandTotal($invoice->getGrandTotal() + $invoice->getLuxuryTaxFee());
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $invoice->getBaseLuxuryTaxFee());

        return $this;
    }
}

<?php

namespace Elogic\LuxuryTax\Model\Invoice\Pdf;

class LuxuryTaxFee extends \Magento\Sales\Model\Order\Pdf\Total\DefaultTotal
{
    /**
     * Get Totals For Display
     *
     * @return array[]
     */
    public function getTotalsForDisplay()
    {
        $amount = $this->getOrder()->formatPriceTxt($this->getOrder()->getLuxuryTaxFee());
        if ($this->getAmountPrefix()) {
            $amount = $this->getAmountPrefix() . $amount;
        }

        $title = __($this->getTitle());
        if ($this->getTitleSourceField()) {
            $label = $title . ' (' . $this->getTitleDescription() . '):';
        } else {
            $label = $title . ':';
        }

        $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;
        $total = ['amount' => $amount, 'label' => $label, 'font_size' => $fontSize];
        return [$total];
    }
}

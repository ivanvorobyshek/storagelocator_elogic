<?php

namespace Elogic\LuxuryTax\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Elogic\LuxuryTax\Model\LuxuryTaxFactory;
use Elogic\LuxuryTax\Model\ResourceModel\LuxuryTax as LuxuryTaxResource;

class SaveToOrderLuxutyTaxFee implements ObserverInterface
{

    /**
     * @var LuxuryTaxFactory
     */
    private LuxuryTaxFactory $luxuryTaxFactory;

    /**
     * @var LuxuryTaxResource
     */
    private LuxuryTaxResource $luxuryTaxResource;

    /**
     * Construct
     *
     * @param LuxuryTaxFactory $luxuryTaxFactory
     * @param LuxuryTaxResource $luxuryTaxResource
     */
    public function __construct(
        LuxuryTaxFactory $luxuryTaxFactory,
        LuxuryTaxResource $luxuryTaxResource
    ) {
        $this->luxuryTaxFactory = $luxuryTaxFactory;
        $this->luxuryTaxResource = $luxuryTaxResource;
    }

    /**
     * Execute
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $event = $observer->getEvent();
        $quote = $event->getQuote();
        $order = $event->getOrder();
        $customerGroupId = $quote->getCustomerGroupId();
        $luxuryTax = $this->luxuryTaxFactory->create();
        $this->luxuryTaxResource->load($luxuryTax, $customerGroupId, 'customer_group');
        $luxuryTaxCondition = $luxuryTax->getData('condition_amount');
        if ($luxuryTaxCondition == 1) {
            $order->setData('luxury_tax_fee', $luxuryTax->getData('tax_rate'));
        } else {
            $order->setData('luxury_tax_fee', 0.0);
        }
    }
}

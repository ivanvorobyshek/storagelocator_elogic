<?php

namespace Elogic\LuxuryTax\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Elogic\LuxuryTax\Model\ResourceModel\LuxuryTax as LuxuryTaxResource;
use Elogic\LuxuryTax\Model\LuxuryTaxFactory;

class CheckCustomerGroupChange implements ObserverInterface
{

    /**
     * @var CustomerRepositoryInterface
     */
    private CustomerRepositoryInterface $customerRepository;

    /**
     * @var LuxuryTaxRepositoryInterface
     */
    private LuxuryTaxRepositoryInterface $luxuryTaxRepository;

    /**
     * @var LuxuryTaxFactory
     */
    private LuxuryTaxFactory $luxuryTaxFactory;

    /**
     * @var LuxuryTaxResource
     */
    private LuxuryTaxResource $luxuryTaxResource;

    /**
     * Construct
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param LuxuryTaxFactory $luxuryTaxFactory
     * @param LuxuryTaxResource $luxuryTaxResource
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        LuxuryTaxFactory $luxuryTaxFactory,
        LuxuryTaxResource $luxuryTaxResource
    ) {
        $this->customerRepository = $customerRepository;
        $this->luxuryTaxFactory = $luxuryTaxFactory;
        $this->luxuryTaxResource = $luxuryTaxResource;
    }

    /**
     * Execute
     *
     * @param Observer $observer
     * @return $this|void
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     */
    public function execute(Observer $observer)
    {
        $data = $observer->getData();
        $customerGroupID = $data['customer_data_object']->getGroupId();
        $prevCustomerGroupID = $data['orig_customer_data_object']->getGroupId();
        if ($customerGroupID === $prevCustomerGroupID) {
            return $this;
        }
        $customer = $this->customerRepository->getById($data['customer_data_object']->getId());
        $luxuryTax = $this->luxuryTaxFactory->create();
        $this->luxuryTaxResource->load($luxuryTax, $customerGroupID, 'customer_group');

        if ($luxuryTax->getData('tax_rate') === null) {
            $taxRate = " ";
        } else {
            $taxRate = 'Name: '. $luxuryTax->getData('name') . ', Rate: '. $luxuryTax->getData('tax_rate')
                . ', Description: ' . $luxuryTax->getData('description');
        }
        $customer->setCustomAttribute('luxury_tax_customer_attribute', $taxRate);
        $this->customerRepository->save($customer);
        return $this;
    }
}

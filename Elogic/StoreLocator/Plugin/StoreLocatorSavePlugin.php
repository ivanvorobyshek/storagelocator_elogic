<?php

namespace Elogic\Storelocator\Plugin;

use Elogic\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;
//use Elogic\StoreLocator\Model\StoreLocator;
use Elogic\StoreLocator\Api\Data\StoreLocatorInterface;
use Elogic\StoreLocator\Model\Geocoder\Geocoder;
use Exception;

class StoreLocatorSavePlugin
{
    /**
     * @var Geocoder
     */
    private Geocoder $geocoder;

    /**
     * @param Geocoder $geocoder
     */
    public function __construct(Geocoder $geocoder)
    {
        $this->geocoder = $geocoder;
    }

    /**
     * Calculates Latitude and Longitude from Address, if they are empty
     *
     * @param StoreLocatorResource $storeLocatorResource
     * @param StoreLocatorInterface $store
     * @return void
     * @throws Exception
     */
    public function beforeSave(StoreLocatorResource $storeLocatorResource, StoreLocatorInterface $store)
    {
        if (!is_numeric($store->getData('latitude')) || !is_numeric($store->getData('longitude'))) {
            $address = 'street '. $store->getData('street') .', '. $store->getData('city')
                . ', '. $store->getData('country');
            $geocoder = $this->geocoder;
            $geocoder->init($address);
            if ($geocoder->getLocation()) {
                $store->setData('longitude', $geocoder->getLng());
                $store->setData('latitude', $geocoder->getLat());
            } else {
                throw new Exception("Geometry location is not defined! Store data was not saved!", 1);
            }
        }
    }
}

<?php

namespace Elogic\Storelocator\Plugin;

use Elogic\StoreLocator\Model\Import\StoreLocatorImport;
use Elogic\StoreLocator\Model\Geocoder\Geocoder;
use Exception;

class StoreLocatorImportPlugin
{
    /**
     * @var Geocoder
     */
    private Geocoder $geocoder;

    /**
     * @param Geocoder $geocoder
     */
    public function __construct(Geocoder $geocoder)
    {
        $this->geocoder = $geocoder;
    }

    /**
     * Calculates Latitude and Longitude from Address, if they are empty
     *
     * @param StoreLocatorImport $storeLocatorImport
     * @param array $entityData
     * @return array
     * @throws Exception
     */
    public function beforeSaveEntityFinish(StoreLocatorImport $storeLocatorImport, array $entityData)
    {
        foreach ($entityData as $key => $value) {
            if (!is_numeric($value[0]['latitude']) || !is_numeric($value[0]['longitude'])) {
                $address = 'street '. $value[0]['street'] .', '. $value[0]['city'] . ', '. $value[0]['country'];
                $geocoder = $this->geocoder;
                $geocoder->init($address);
                if ($geocoder->getLocation()) {
                    $entityData[$key][0]['latitude'] = $geocoder->getLat();
                    $entityData[$key][0]['longitude'] = $geocoder->getLng();
                }
            }
        }
        $r[0] = $entityData;
        return $r;
    }
}

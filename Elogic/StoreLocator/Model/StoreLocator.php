<?php

namespace Elogic\StoreLocator\Model;

use Elogic\StoreLocator\Model\ResourceModel\StoreLocator as ResourceModel;
use Magento\Framework\Model\AbstractModel;
use Elogic\StoreLocator\Api\Data\StoreLocatorInterface;

class StoreLocator extends AbstractModel implements StoreLocatorInterface
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'stores_locator_db_model';

    /**
     * Initialize magento model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * Get Sore ID
     *
     * @return string
     */
    public function getEntityId(): string
    {
        return (string)$this->getData(self::STORE_ID);
    }

    /**
     * Set Store ID
     *
     * @param $id
     * @return StoreLocatorInterface
     */
    public function setEntityId($id): StoreLocatorInterface
    {
        return $this->setData(self::STORE_ID, (int) $id);
    }

    /**
     * Get Store Name
     *
     * @return string
     */
    public function getName(): string
    {
        return (string)$this->getData(self::STORE_NAME);
    }

    /**
     * Set Store Name
     *
     * @param string $name
     * @return StoreLocatorInterface
     */
    public function setName(string $name): StoreLocatorInterface
    {
        return $this->setData(self::STORE_NAME, $name);
    }

    /**
     * Get Description
     *
     * @return string
     */
    public function getDescription(): string
    {
        return (string)$this->getData(self::STORE_DESCRIPTION);
    }

    /**
     * Set Description
     *
     * @param string $description
     * @return StoreLocatorInterface
     */
    public function setDescription(string $description): StoreLocatorInterface
    {
        return $this->setData(self::STORE_DESCRIPTION, $description);
    }

    /**
     * Get Image
     *
     * @return string
     */
    public function getImage(): string
    {
        return (string)$this->getData(self::STORE_IMAGE);
    }

    /**
     * Set Image
     *
     * @param string $image
     * @return StoreLocatorInterface
     */
    public function setImage(string $image): StoreLocatorInterface
    {
        return $this->setData(self::STORE_IMAGE, $image);
    }

    /**
     * Get Store Street
     *
     * @return string
     */
    public function getStreet(): string
    {
        return (string)$this->getData(self::STORE_STREET);
    }

    /**
     * Set Store Street
     *
     * @param string $street
     * @return StoreLocatorInterface
     */
    public function setStreet(string $street): StoreLocatorInterface
    {
        return $this->setData(self::STORE_STREET, $street);
    }

    /**
     * Get Store City
     *
     * @return string
     */
    public function getCity(): string
    {
        return (string)$this->getData(self::STORE_CITY);
    }

    /**
     * Set Store City
     *
     * @param string $city
     * @return StoreLocatorInterface
     */
    public function setCity(string $city): StoreLocatorInterface
    {
        return $this->setData(self::STORE_CITY, $city);
    }

    /**
     * Get Store Region
     *
     * @return string
     */
    public function getRegion(): string
    {
        return (string)$this->getData(self::STORE_REGION);
    }

    /**
     * Set Store Region
     *
     * @param string $region
     * @return StoreLocatorInterface
     */
    public function setRegion(string $region): StoreLocatorInterface
    {
        return $this->setData(self::STORE_REGION, $region);
    }

    /**
     * Get Store Zip
     *
     * @return string
     */
    public function getZip(): string
    {
        return (string)$this->getData(self::STORE_ZIP);
    }

    /**
     * Set Store Zip
     *
     * @param string $zip
     * @return StoreLocatorInterface
     */
    public function setZip(string $zip): StoreLocatorInterface
    {
        return $this->setData(self::STORE_ZIP, $zip);
    }

    /**
     * Get Store Country
     *
     * @return string
     */
    public function getCountry(): string
    {
        return (string)$this->getData(self::STORE_COUNTRY);
    }

    /**
     * Set Store Country
     *
     * @param string $country
     * @return StoreLocatorInterface
     */
    public function setCountry(string $country): StoreLocatorInterface
    {
        return $this->setData(self::STORE_COUNTRY, $country);
    }

    /**
     * Get Store Work Schedule
     *
     * @return string
     */
    public function getWorkSchedule(): string
    {
        return (string)$this->getData(self::STORE_WORK_SCHEDULE);
    }

    /**
     * Set Store Work Schedule
     *
     * @param string $workSchedule
     * @return StoreLocatorInterface
     */
    public function setWorkSchedule(string $workSchedule): StoreLocatorInterface
    {
        return $this->setData(self::STORE_WORK_SCHEDULE, $workSchedule);
    }

    /**
     * Get Store Longitude
     *
     * @return float
     */
    public function getLongitude(): float
    {
        return (float)$this->getData(self::STORE_LONGITUDE);
    }

    /**
     * Set Store Longitude
     *
     * @param $longitude
     * @return StoreLocatorInterface
     */
    public function setLongitude($longitude): StoreLocatorInterface
    {
        return $this->setData(self::STORE_LONGITUDE, $longitude);
    }

    /**
     * Get Store Latitude
     *
     * @return float
     */
    public function getLatitude(): float
    {
        return (float)$this->getData(self::STORE_LATITUDE);
    }

    /**
     * Set Store Latitude
     *
     * @param $latitude
     * @return StoreLocatorInterface
     */
    public function setLatitude($latitude): StoreLocatorInterface
    {
        return $this->setData(self::STORE_LATITUDE, $latitude);
    }

    /**
     * Get Store Url Key
     *
     * @return string
     */
    public function getStoreUrlKey(): string
    {
        return (string)$this->getData(self::STORE_URL_KEY);
    }

    /**
     * Set Store Url Key
     *
     * @param string $storeUrlKey
     * @return StoreLocatorInterface
     */
    public function setStoreUrlKey(string $storeUrlKey): StoreLocatorInterface
    {
        return $this->setData(self::STORE_URL_KEY, $storeUrlKey);
    }
}

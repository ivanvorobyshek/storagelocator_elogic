<?php

namespace Elogic\StoreLocator\Model;

use Elogic\StoreLocator\Api\Data\StoreLocatorInterface;
//use Magento\Framework\Exception\NoSuchEntityException;
use Elogic\StoreLocator\Api\StoreLocatorRepositoryInterface;
use Elogic\StoreLocator\Model\ResourceModel\StoreLocator\CollectionFactory;
use Elogic\StoreLocator\Model\StoreLocatorFactory;
use Elogic\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;

class StoreLocatorRepository implements StoreLocatorRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var StoreLocatorFactory
     */
    private StoreLocatorFactory $storeLocatorFactory;

    /**
     * @var StoreLocatorResource
     */
    private StoreLocatorResource $storeLocatorResource;

    /**
     * @param CollectionFactory $collectionFactory
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param StoreLocatorResource $storeLocatorResource
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        StoreLocatorFactory $storeLocatorFactory,
        StoreLocatorResource $storeLocatorResource
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->storeLocatorResource = $storeLocatorResource;
    }

    /**
     * Get List of Stores
     *
     * @return array|StoreLocatorInterface
     */
    public function getList(): array
    {
        return $this->collectionFactory->create()->getItems();
    }

    /**
     * Get Store
     *
     * @param int $id
     * @return StoreLocatorInterface
     * @throws \Exception
     */
    public function get(int $id): StoreLocatorInterface
    {
        $store = $this->storeLocatorFactory->create();
        $this->storeLocatorResource->load($store, $id);
        if (!$store->getId()) {
            throw new \Exception(__('Requested store does not exist'));
        }
        return $store;
    }

    /**
     * Get Store By URL
     *
     * @param string $url
     * @return StoreLocatorInterface
     * @throws \Exception
     */
    public function getStoreByUrl(string $url): StoreLocatorInterface
    {
        $store = $this->storeLocatorFactory->create();
        $this->storeLocatorResource->load($store, $url, StoreLocatorInterface::STORE_URL_KEY);
        if ($store->getData(StoreLocatorInterface::STORE_URL_KEY)) {
            return $store;
        }
        throw new \Exception(__('Requested store with URL %1 does not exist', $url));
    }

    /**
     * Save Store
     *
     * @param string $name
     * @param string $description
     * @param string $image
     * @param string $street
     * @param string $city
     * @param string $region
     * @param string $zip
     * @param string $country
     * @param string $work_schedule
     * @param string $longitude
     * @param string $latitude
     * @param string $storeUrlKey
     * @param int|null $id
     * @return bool
     * @throws \Exception
     */
    public function save(
        string $name,
        string $description,
        string $image,
        string $street,
        string $city,
        string $region,
        string $zip,
        string $country,
        string $work_schedule,
        string $longitude,
        string $latitude,
        string $storeUrlKey,
        ?int $id = null
    ): bool {
        try {
            if ($id === null) {
                $store = $this->storeLocatorFactory->create();
            } else {
                $store = $this->get($id);
            }
            $store->setName($name);
            $store->setDescription($description);
            $store->setImage($image);
            $store->setStreet($street);
            $store->setCity($city);
            $store->setRegion($region);
            $store->setZip($zip);
            $store->setCountry($country);
            $store->setWorkSchedule($work_schedule);
            $store->setLongitude($longitude);
            $store->setLatitude($latitude);
            $store->setStoreUrlKey($storeUrlKey);
            $this->storeLocatorResource->save($store);
        } catch (\Exception $e) {
            throw new \Exception(__('Unable to save store. %1', $e->getMessage()));
        }
        return true;
    }

    /**
     * Delete Store
     *
     * @param StoreLocatorInterface $store
     * @return bool
     * @throws \Exception
     */
    public function delete(StoreLocatorInterface $store): bool
    {
        try {
            $this->storeLocatorResource->delete($store);
        } catch (\Exception $e) {
            throw new \Exception(__('Unable to remove store #%1', $store->getId()));
        }
        return true;
    }

    /**
     * Delete Store By ID
     *
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function deleteById(int $id): bool
    {
        return $this->delete($this->get($id));
    }

    /**
     * Save Store like Object
     *
     * @param StoreLocatorInterface $store
     * @return StoreLocatorInterface
     * @throws \Exception
     */
    public function saveObj(StoreLocatorInterface $store): StoreLocatorInterface
    {
        try {
            $this->storeLocatorResource->save($store);
        } catch (\Exception $e) {
            throw new \Exception(__('Unable to save store. %1', $e->getMessage()));
        }
        return $store;
    }
}

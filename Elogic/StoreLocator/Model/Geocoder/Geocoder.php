<?php

namespace Elogic\StoreLocator\Model\Geocoder;

use Elogic\StoreLocator\Model\Config;
use Exception;

class Geocoder
{

    /**
     * Google Maps Geocode Service API url
     * @var string
     */
    private $api_url = "https://maps.google.com/maps/api/geocode/json";

    /**
     * your own Google Maps API key
     * @var string
     * @see https://developers.google.com/maps/documentation/javascript/get-api-key
     */
    private $key = '';

    /**
     * Google Maps API sensor
     * @var string - true|false
     */
    private $sensor = 'false';

    /**
     * Address to encode - transform in Lat/Lan
     * @var string
     */
    private $address = '';

    /**
     * CURL response
     * @var string
     */
    private $response = null;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * latitude
     *
     * @var float
     */
    private float $lat = 0;

    /**
     * Longitude
     *
     * @var float
     */
    private float $lng = 0;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Init Geocoder
     *
     * @param string $address
     * @param string $sensor
     * @return void
     */
    public function init(string $address, string $sensor = 'false')
    {
        $this->setAddress($address);
        $this->setSensor($sensor);
        if ($this->config->isEnabled()) {
            $this->key = $this->config->getGoogleApiKey();
        }
        $this->call();
    }

    /**
     * Set address to encode
     *
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * Set sensor parameter
     *
     * @param $sensor mixed string|bool
     * accepted true|false - "true"|"false"
     */
    public function setSensor($sensor)
    {
        if ($sensor === false) {
            $sensor = 'false';
        }
        $this->sensor = $sensor;
    }

    /**
     * Get Longitude and Latitude
     *
     * @return bool
     * @throws Exception
     */
    public function getLocation()
    {
        try {
            if (!empty($this->response['status']) && $this->response['status'] == 'OK') {
                $args = $this->response['results'][0]['geometry']['location'];
                if (count($args) === 1) {
                    if (!empty($args['lat'])) {
                        $this->lat = $args['lat'];
                    }
                    if (!empty($args['lng'])) {
                        $this->lng = $args['lng'];
                    }
                } elseif (count($args) === 2) {
                    $this->lat = $args['lat'];
                    $this->lng = $args['lng'];
                }
                return true;
            }
        } catch (Exception $e) {
            throw new Exception(__("Geometry location is not defined"));
        }
        return false;
    }

    /**
     * Return the coomplete Google Maps API URL
     *
     * @return string
     */
    public function getUrl()
    {
        $attributes = [
            'sensor' => $this->sensor,
            'address' => $this->address,
        ];
        $query = http_build_query($attributes);
        return $this->api_url . '?' . $query . '&key='. $this->key;
    }

    /**
     * Perform the Google Maps API call
     *
     * @return array - decoded JSON
     */
    private function call()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $this->getUrl());
        $json_response = curl_exec($curl);
        curl_close($curl);
        $this->response = json_decode($json_response, true);
    }

    /**
     * Return the latitude
     *
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * Return the longitude
     *
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }
}

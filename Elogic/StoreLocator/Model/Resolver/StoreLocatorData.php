<?php

namespace Elogic\StoreLocator\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Elogic\StoreLocator\Api\StoreLocatorRepositoryInterface;

class StoreLocatorData implements ResolverInterface
{

    /**
     * @var StoreLocatorRepositoryInterface
     */
    private StoreLocatorRepositoryInterface $storeLocatorRepository;

    /**
     * Construct
     *
     * @param StoreLocatorRepositoryInterface $storeLocatorRepository
     */
    public function __construct(StoreLocatorRepositoryInterface $storeLocatorRepository)
    {
        $this->storeLocatorRepository = $storeLocatorRepository;
    }

    /**
     * Resolve
     *
     * @param Field $field
     * @param \Magento\Framework\GraphQl\Query\Resolver\ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array|\Magento\Framework\GraphQl\Query\Resolver\Value|mixed
     * @throws GraphQlInputException
     */
    public function resolve(
        Field       $field,
        $context,
        ResolveInfo $info,
        array       $value = null,
        array       $args = null
    ) {
        if (!isset($args['id']) || empty($args['id'] || ((int)$args['id'] == ""))) {
            throw new GraphQlInputException(__('Invalid Store ID.'));
        }
        $output = [];
        try {
            $store = $this->storeLocatorRepository->get($args['id']);
            $output['id'] = $args['id'];
            $output['name'] = $store->getName();
            $output['error'] = 'There is no error';
        } catch (\Exception $e) {
            $output['error'] = 'There is no store with such id = ' . $args['id'];
        }
        return $output;
    }
}

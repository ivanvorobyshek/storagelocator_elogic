<?php

namespace Elogic\StoreLocator\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Config
{
    public const XML_PATH_ENABLED = 'storeslocator/general/enabled';
    public const XML_PATH_GOOGLE_API_KEY = 'storeslocator/general/apikey';

    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $config;

    /**
     * @param ScopeConfigInterface $config
     */
    public function __construct(
        ScopeConfigInterface $config
    ) {
        $this->config = $config;
    }

    /**
     * Check if Module is enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->config->isSetFlag(self::XML_PATH_ENABLED);
    }

    /**
     * Get Google API Key
     *
     * @return mixed
     */
    public function getGoogleApiKey()
    {
        return $this->config->getValue(self::XML_PATH_GOOGLE_API_KEY);
    }
}

<?php

namespace Elogic\StoreLocator\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class StoreLocator extends AbstractDb
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'stores_locator_db_resource_model';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('stores_locator_db', 'entity_id');
        $this->_useIsObjectNew = true;
    }
}

<?php

namespace Elogic\StoreLocator\Model\ResourceModel\StoreLocator;

use Elogic\StoreLocator\Model\ResourceModel\StoreLocator as ResourceModel;
use Elogic\StoreLocator\Model\StoreLocator as Model;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'stores_locator_db_collection';

    /**
     * Initialize collection model.
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}

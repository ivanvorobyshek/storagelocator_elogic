<?php

namespace Elogic\StoreLocator\Api\Data;

interface StoreLocatorInterface
{
    public const STORE_ID = 'entity_id';
    public const STORE_NAME = 'name';
    public const STORE_DESCRIPTION = 'description';
    public const STORE_IMAGE = 'image';
    public const STORE_STREET = 'street';
    public const STORE_CITY = 'city';
    public const STORE_REGION = 'region';
    public const STORE_ZIP = 'zip';
    public const STORE_COUNTRY = 'country';
    public const STORE_WORK_SCHEDULE = 'work_schedule';
    public const STORE_LONGITUDE = 'longitude';
    public const STORE_LATITUDE = 'latitude';

    public const STORE_URL_KEY = 'store_url_key';

    /**
     * Get Store ID
     *
     * @return string
     */
    public function getEntityId(): string;

    /**
     * Set Store ID
     *
     * @param int $id
     * @return StoreLocatorInterface
     */
    public function setEntityId($id): StoreLocatorInterface;

    /**
     * Get Store Name
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Set Store Name
     *
     * @param string $name
     * @return StoreLocatorInterface
     */
    public function setName(string $name): StoreLocatorInterface;

    /**
     * Get Store Description
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Set Store Description
     *
     * @param string $description
     * @return string
     */
    public function setDescription(string $description): StoreLocatorInterface;

    /**
     * Get Store Image
     *
     * @return string
     */
    public function getImage(): string;

    /**
     * Set Store Image
     *
     * @param string $image
     * @return StoreLocatorInterface
     */
    public function setImage(string $image): StoreLocatorInterface;

    /**
     * Get Store Street
     *
     * @return string
     */
    public function getStreet(): string;

    /**
     * Get Store Street
     *
     * @param string $street
     * @return StoreLocatorInterface
     */
    public function setStreet(string $street): StoreLocatorInterface;

    /**
     * Get Store City
     *
     * @return string
     */
    public function getCity(): string;

    /**
     * Set Store City
     *
     * @param string $city
     * @return StoreLocatorInterface
     */
    public function setCity(string $city): StoreLocatorInterface;

    /**
     * Get Store Region
     *
     * @return string
     */
    public function getRegion(): string;

    /**
     * Set Store Region
     *
     * @param string $region
     * @return StoreLocatorInterface
     */
    public function setRegion(string $region): StoreLocatorInterface;

    /**
     * Get Store Zip
     *
     * @return string
     */
    public function getZip(): string;

    /**
     * Set Store Zip
     *
     * @param string $zip
     * @return StoreLocatorInterface
     */
    public function setZip(string $zip): StoreLocatorInterface;

    /**
     * Get Store County
     *
     * @return string
     */
    public function getCountry(): string;

    /**
     * Set Store Country
     *
     * @param string $country
     * @return StoreLocatorInterface
     */
    public function setCountry(string $country): StoreLocatorInterface;

    /**
     * Get Store Work Schedule
     *
     * @return string
     */
    public function getWorkSchedule(): string;

    /**
     * Set Store Work Schedule
     *
     * @param string $workSchedule
     * @return StoreLocatorInterface
     */
    public function setWorkSchedule(string $workSchedule): StoreLocatorInterface;

    /**
     * Get Store Longitude
     *
     * @return float
     */
    public function getLongitude(): float;

    /**
     * Set Store Longitude
     *
     * @param $longitude
     * @return StoreLocatorInterface
     */
    public function setLongitude($longitude): StoreLocatorInterface;

    /**
     * Get Store Latitude
     *
     * @return float
     */
    public function getLatitude(): float;

    /**
     * Set Store Latitude
     *
     * @param $latitude
     * @return StoreLocatorInterface
     */
    public function setLatitude($latitude): StoreLocatorInterface;

    /**
     * Get Store URL Key
     *
     * @return string
     */
    public function getStoreUrlKey(): string;

    /**
     * Set Store URL Key
     *
     * @param string $storeUrlKey
     * @return StoreLocatorInterface
     */
    public function setStoreUrlKey(string $storeUrlKey): StoreLocatorInterface;
}

<?php

namespace Elogic\StoreLocator\Api;

use Elogic\StoreLocator\Api\Data\StoreLocatorInterface;

interface StoreLocatorRepositoryInterface
{


    /**
     * Get Store
     *
     * @param  integer $id
     * @return StoreLocatorInterface
     */
    public function get(int $id): StoreLocatorInterface;


    /**
     * Get List Of Stores
     *
     * @return StoreLocatorInterface[]
     */
    public function getList(): array;


    /**
     * Save Store By Params
     *
     * @param  string       $name
     * @param  string       $description
     * @param  string       $image
     * @param  string       $street
     * @param  string       $city
     * @param  string       $region
     * @param  string       $zip
     * @param  string       $country
     * @param  string       $work_schedule
     * @param  string       $longitude
     * @param  string       $latitude
     * @param  string       $storeUrlKey
     * @param  integer|null $id
     * @return boolean
     */
    public function save(
        string $name,
        string $description,
        string $image,
        string $street,
        string $city,
        string $region,
        string $zip,
        string $country,
        string $work_schedule,
        string $longitude,
        string $latitude,
        string $storeUrlKey,
        ?int $id=null
    ): bool;


    /**
     * Delete Store
     *
     * @param  StoreLocatorInterface $store
     * @return boolean
     */
    public function delete(StoreLocatorInterface $store): bool;


    /**
     * Delete Store By ID
     *
     * @param  integer $id
     * @return boolean
     */
    public function deleteById(int $id): bool;


    /**
     * Save Store Like Object
     *
     * @param  StoreLocatorInterface $store
     * @return StoreLocatorInterface
     */
    public function saveObj(StoreLocatorInterface $store): StoreLocatorInterface;


}//end interface

<?php

namespace Elogic\StoreLocator\ViewModel;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Elogic\StoreLocator\Model\StoreLocatorRepository;
use Elogic\StoreLocator\Api\Data\StoreLocatorInterface;
use Elogic\StoreLocator\Model\Config;

class ViewStore implements ArgumentInterface
{

    /**
     * @var RequestInterface
     */
    private RequestInterface $request;

    /**
     * @var StoreLocatorRepository
     */
    private StoreLocatorRepository $storeLocatorRepository;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @param RequestInterface $request
     * @param StoreLocatorRepository $storeLocatorRepository
     * @param Config $config
     * @param array $data
     */
    public function __construct(
        RequestInterface $request,
        StoreLocatorRepository $storeLocatorRepository,
        Config $config,
        array $data = []
    ) {
        $this->config = $config;
        $this->request = $request;
        $this->storeLocatorRepository = $storeLocatorRepository;
    }

    /**
     * Get Data
     *
     * @return StoreLocatorInterface|null
     * @throws \Exception
     */
    public function getOutData()
    {
        $url = $this->request->getParam(StoreLocatorInterface::STORE_URL_KEY, null);
        if ($url !== null && $url !== '') {
            $store = $this->storeLocatorRepository->getStoreByUrl($url);
            $store['key'] = $this->config->getGoogleApiKey();
            $img = explode("|", $store->getImage());
            if (isset($img[0])) {
                $store->setImage($img[0]);
            }
            return $store;
        }
        return null;
    }
}

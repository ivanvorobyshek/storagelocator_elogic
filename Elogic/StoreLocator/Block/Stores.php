<?php

namespace Elogic\StoreLocator\Block;

use Elogic\StoreLocator\Api\Data\StoreLocatorInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Elogic\StoreLocator\Model\ResourceModel\StoreLocator\Collection;

class Stores extends Template
{

    /**
     * @var Collection
     */
    private Collection $collection;

    /**
     * @param Context $context
     * @param Collection $collection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Collection $collection,
        array $data = []
    ) {
        $this->collection = $collection;
        parent::__construct($context, $data);
    }

    /**
     * Get Store Collection
     *
     * @return Collection
     */
    public function getCollection(): Collection
    {
        $this->collection->addFieldToSelect(['name', 'description', 'country',
            'image', 'store_url_key', 'city', 'street']);
        return $this->collection;
    }

    /**
     * Prepare Layout
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Stores'));
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'stores.pager'
            )->setAvailableLimit([5=>5,10=>10,15=>15,20=>20,30=>30])->setShowPerPage(true)
                ->setCollection($this->getCollection());
            $this->setChild('pager', $pager);
            $this->getCollection()->load();
        return $this;
    }

    /**
     * Get Pager
     *
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Get Store Image
     *
     * @param StoreLocatorInterface $store
     * @return string
     */
    public function getImg(StoreLocatorInterface $store)
    {
        $img = explode("|", $store->getImage());
        return isset($img[0]) ? $img[0] : 'path to default picture';
    }
}

<?php
declare(strict_types=1);

namespace Elogic\StoreLocator\Router;

use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;
use Elogic\StoreLocator\Api\Data\StoreLocatorInterface;

class StoreRouter implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    private $actionFactory;

    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * Router constructor.
     *
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response
    ) {
        $this->actionFactory = $actionFactory;
        $this->response = $response;
    }

    /**
     * Check URL
     *
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request): ?ActionInterface
    {
        $identifier = trim($request->getPathInfo(), '/');
        if (strpos($identifier, 'stores') !== false && $request->getActionName() !== 'defaultNoRoute') {
            $url = explode("/", $identifier);
            if ($url[0] === 'stores') {
                $request->setModuleName('stores');
                $request->setControllerName('index');
                if (isset($url[1])) {
                    $request->setActionName('store');
                    $request->setParams([
                        StoreLocatorInterface::STORE_URL_KEY => $url[1],
                    ]);
                } else {
                    $request->setActionName('index');
                }
                return $this->actionFactory->create(Forward::class, ['request' => $request]);
            }
        }
        return null;
    }
}

<?php

namespace Elogic\StoreLocator\Console\Command;

use Magento\Framework\Console\Cli;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Magento\Framework\App\Filesystem\DirectoryList;
use Elogic\StoreLocator\Model\StoreLocatorFactory;
use Elogic\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;

class LoadStore extends Command
{
    /**
     * @var StoreLocatorFactory
     */
    private StoreLocatorFactory $storeLocatorFactory;

    /**
     * @var StoreLocatorResource
     */
    private StoreLocatorResource $storeLocatorResource;

    /**
     * @var DirectoryList
     */
    private DirectoryList $directoryList;

    public const INPUT_KEY_FILENAME = 'filename';

    public const INPUT_KEY_NUMBER_OF_ROWS = 'number_of_rows';

    /**
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param StoreLocatorResource $storeLocatorResource
     * @param DirectoryList $directoryList
     * @param string|null $name
     */
    public function __construct(
        StoreLocatorFactory $storeLocatorFactory,
        StoreLocatorResource $storeLocatorResource,
        DirectoryList $directoryList,
        string $name = null
    ) {
        $this->storeLocatorResource = $storeLocatorResource;
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->directoryList = $directoryList;
        parent::__construct($name);
    }

    /**
     * Initialization of the command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('loadstore')
            ->addArgument(
                self::INPUT_KEY_FILENAME,
                InputArgument::REQUIRED,
                'File name'
            )
            ->addArgument(
                self::INPUT_KEY_NUMBER_OF_ROWS,
                InputArgument::REQUIRED,
                'Number of rows to import'
            );
        $this->setDescription('Load Stores From CSV File');
        parent::configure();
    }

    /**
     * CLI command description.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fileName = $input->getArgument(self::INPUT_KEY_FILENAME);
        $rowsNum = (int)$input->getArgument(self::INPUT_KEY_NUMBER_OF_ROWS);
        if ($fileName !== "") {
            if ($rowsNum >= 1) {
                $func = $this->getFileContents($fileName, $rowsNum);
            } else {
                $func = "Specify the number of lines in the file to be read. It must be greater than 1";
            }
        } else {
            $func = "File name must not be empty";
        }
        $output->writeln($func);
    }

    /**
     * Get Data From File
     *
     * @param string $fileName
     * @param int $rowsNum
     * @return array
     */
    public function getFileContents(string $fileName, int $rowsNum = 1)
    {
        $errors = [];
        $row = 1;
        try {
            $path = $this->directoryList->getPath(DirectoryList::MEDIA) . '/' . $fileName;
            if (file_exists($path)) {
                $csv = [];
                if (($handle = fopen($path, "r")) !== false) {
                    while ((($data = fgetcsv($handle, null, ",")) !== false) && $row <= $rowsNum) {
                        if ($data[0] === "id") {
                            continue;
                        }
                        $row++;
                        $csv[] = $data;
                    }
                    fclose($handle);
                }
            } else {
                $errors[] = "The file $path does not exist";
            }
        } catch (\Exception $e) {
            $errors[] = "Could not read store data from row $row. ". $e->getMessage();
        }
        $row = 0;
        $storesAdded = 0;
        foreach ($csv as $csv_store) {
            try {
                $row++;
                $store = $this->storeLocatorFactory->create();
                $store->setName($csv_store[1]);
                $store->setDescription($csv_store[8]);
                $store->setImage($csv_store[12]);
                $store->setStreet($csv_store[5]);
                $store->setCity($csv_store[3]);
                $store->setRegion($csv_store[7]);
                $store->setZip($csv_store[4]);
                $store->setCountry($csv_store[2]);
                $store->setWorkSchedule($csv_store[14]);
                $store->setStoreUrlKey($csv_store[17]);
                $latLong = $csv_store[6];
                $ll = explode(" ", $latLong);
                if (isset($ll[0], $ll[1])) {
                    $store->setLatitude($ll[0]);
                    $store->setLongitude($ll[1]);
                }
                $this->storeLocatorResource->save($store);
                $storesAdded++;
            } catch (\Exception $e) {
                $errors[] = "Unable to save store data from row $row. ". $e->getMessage();
            }
        }
        $errors[] = "It is OK. $storesAdded store(s) added.";
        return $errors;
    }
}

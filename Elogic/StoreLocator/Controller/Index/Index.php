<?php
declare(strict_types=1);

namespace Elogic\StoreLocator\Controller\Index;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Elogic\StoreLocator\Model\Config;
use Magento\Framework\Controller\Result\ForwardFactory;

class Index implements HttpGetActionInterface
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var ForwardFactory
     */
    private ForwardFactory $forwardFactory;

    /**
     * @param PageFactory $pageFactory
     * @param RequestInterface $request
     * @param Config $config
     * @param ForwardFactory $forwardFactory
     */
    public function __construct(
        PageFactory $pageFactory,
        RequestInterface $request,
        Config $config,
        ForwardFactory $forwardFactory
    ) {
        $this->pageFactory = $pageFactory;
        $this->request = $request;
        $this->config = $config;
        $this->forwardFactory = $forwardFactory;
    }

    /**
     * Load page or 404 (if Module is disabled)
     *
     * @return ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {
        if ($this->config->isEnabled()) {
            return $this->pageFactory->create();
        }
        return $this->forwardFactory->create()->forward('defaultNoRoute');
    }
}

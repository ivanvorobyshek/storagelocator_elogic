<?php

namespace Elogic\StoreLocator\Controller\Index;

use Elogic\StoreLocator\Api\Data\StoreLocatorInterface;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Elogic\StoreLocator\Model\Config;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\ForwardFactory;
use Elogic\StoreLocator\Model\StoreLocatorFactory;
use Elogic\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;

class Store implements HttpGetActionInterface
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ForwardFactory
     */
    private ForwardFactory $forwardFactory;

    /**
     * @var StoreLocatorFactory
     */
    private StoreLocatorFactory $storeLocatorFactory;

    /**
     * @var StoreLocatorResource
     */
    private StoreLocatorResource $storeLocatorResource;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @param PageFactory $pageFactory
     * @param RequestInterface $request
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param StoreLocatorResource $storeLocatorResource
     * @param ForwardFactory $forwardFactory
     * @param Config $config
     */
    public function __construct(
        PageFactory $pageFactory,
        RequestInterface $request,
        StoreLocatorFactory $storeLocatorFactory,
        StoreLocatorResource $storeLocatorResource,
        ForwardFactory $forwardFactory,
        Config $config
    ) {
        $this->forwardFactory = $forwardFactory;
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->storeLocatorResource = $storeLocatorResource;
        $this->pageFactory = $pageFactory;
        $this->request = $request;
        $this->config = $config;
    }

    /**
     * Check if there is the store in DB with such store_url_key
     *
     * @param string $storeUrl
     * @return bool
     */
    public function loadStoreByUrl(string $storeUrl): bool
    {
        $store = $this->storeLocatorFactory->create();
        $this->storeLocatorResource->load($store, $storeUrl, StoreLocatorInterface::STORE_URL_KEY);
        if ($store->getData(StoreLocatorInterface::STORE_URL_KEY)) {
            return true;
        }
        return false;
    }

    /**
     * Load Store Page or 404 (if there is no such store)
     *
     * @return ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {
        if ($this->config->isEnabled()) {
            // Get the params that were passed from our Router
            $storeUrl = $this->request->getParam(StoreLocatorInterface::STORE_URL_KEY, null);
            if ($storeUrl === null || $storeUrl === '' || !$this->loadStoreByUrl($storeUrl)) {
                return $this->forwardFactory->create()->forward('defaultNoRoute');
            }
            return $this->pageFactory->create();
        }
        return $this->forwardFactory->create()->forward('defaultNoRoute');
    }
}

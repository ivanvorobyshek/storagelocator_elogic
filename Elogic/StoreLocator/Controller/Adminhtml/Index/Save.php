<?php

namespace Elogic\StoreLocator\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Elogic\StoreLocator\Model\StoreLocatorFactory;
use Elogic\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;
use Magento\Framework\Controller\ResultFactory;

class Save extends Action
{

    public const ADMIN_RESOURCE = 'Elogic_StoreLocator::storeslocator';

    /**
     * @var StoreLocatorFactory
     */
    private StoreLocatorFactory $storeLocatorFactory;

    /**
     * @var StoreLocatorResource
     */
    private StoreLocatorResource $storeLocatorResource;


    /**
     * @param Context              $context
     * @param StoreLocatorFactory  $storeLocatorFactory
     * @param StoreLocatorResource $storeLocatorResource
     */
    public function __construct(
        Context $context,
        StoreLocatorFactory $storeLocatorFactory,
        StoreLocatorResource $storeLocatorResource
    ) {
        $this->storeLocatorFactory  = $storeLocatorFactory;
        $this->storeLocatorResource = $storeLocatorResource;
        parent::__construct($context);

    }//end __construct()


    /**
     * Save Store
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $data   = $this->getRequest()->getPostValue();
        $params = $data['general'];
        if (isset($params['store_image'][0]['url'], $params['store_image'][0]['name'])
            && !isset($params['store_image'][0]['file'])
        ) {
            $params['store_image'][0]['file'] = $params['store_image'][0]['name'];
        }

        if (isset($params['store_image'][0]['url'], $params['store_image'][0]['file'], $params['store_image'][0]['size'])) {
            $store_image = $params['store_image'][0]['url'].'|'.$params['store_image'][0]['file'].'|'.$params['store_image'][0]['size'];
        } else {
            $store_image = '';
        }

        // go to the listing page, if there is no params
        if (!$params) {
            $this->messageManager->addErrorMessage(__('Could not save store data.'));
        }

        if (isset($params['entity_id'])) {
            // load existing store
            $id    = (int) $params['entity_id'];
            $store = $this->storeLocatorFactory->create();
            $this->storeLocatorResource->load($store, $id);
        } else {
            // add new store
            $store = $this->storeLocatorFactory->create();
        }

        try {
            $store->setName($params['name']);
            $store->setDescription($params['description']);
//            if ($store_image !== '') {
//                $store->setImage($store_image);
//            }
            $store->setImage($store_image);
            $store->setStreet($params['street']);
            $store->setCity($params['city']);
            $store->setRegion($params['region']);
            $store->setZip($params['zip']);
            $store->setCountry($params['country']);
            $store->setWorkSchedule($params['work_schedule']);
            // make check for longitude and latitude //maybe plugin??? before storeLocatorResource -> save();
            $store->setLongitude($params['longitude']);
            $store->setLatitude($params['latitude']);
            $store->setStoreUrlKey($params['store_url_key']);
            $this->storeLocatorResource->save($store);
            $this->messageManager->addSuccessMessage(__('Store data has been successfully saved.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }//end try

        $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $result->setPath('*/*/index');
        return $result;
    }
}

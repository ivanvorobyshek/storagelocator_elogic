<?php

namespace Elogic\StoreLocator\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Elogic\StoreLocator\Model\StoreLocatorFactory;
use Elogic\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;
use Magento\Framework\Controller\ResultInterface;

class Delete extends Action
{

    public const ADMIN_RESOURCE = 'Elogic_StoreLocator::storeslocator';

    /**
     * @var StoreLocatorFactory
     */
    private StoreLocatorFactory $storeLocatorFactory;

    /**
     * @var StoreLocatorResource
     */
    private StoreLocatorResource $storeLocatorResource;

    /**
     * @param Context $context
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param StoreLocatorResource $storeLocatorResource
     */
    public function __construct(
        Context $context,
        StoreLocatorFactory $storeLocatorFactory,
        StoreLocatorResource $storeLocatorResource
    ) {
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->storeLocatorResource = $storeLocatorResource;
        parent::__construct($context);
    }

    /**
     * Delete Store
     *
     * @return ResponseInterface|Redirect|(Redirect&ResultInterface)|ResultInterface
     */
    public function execute()
    {
        $id = (int)$this->getRequest()->getParam('id');
        if (!$id) {
            $this->messageManager->addErrorMessage(__('There is no such store to delete.'));
        } else {
            try {
                $store = $this->storeLocatorFactory->create();
                $this->storeLocatorResource->load($store, $id);
                $this->storeLocatorResource->delete($store);
                $this->messageManager->addSuccessMessage(__('Store data has been successfully deleted.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('Something is wrong! Can\'t delete data from DB!'));
            }
        }

        $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $result->setPath('*/*/index');
        return $result;
    }
}

<?php

namespace Elogic\StoreLocator\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Elogic\StoreLocator\Api\StoreLocatorRepositoryInterface;

class MassDelete extends Action
{

    public const ADMIN_RESOURCE = 'Elogic_StoreLocator::storeslocator';

    /**
     * @var StoreLocatorRepositoryInterface
     */
    private StoreLocatorRepositoryInterface $storeLocatorRepository;

    /**
     * @param Context $context
     * @param StoreLocatorRepositoryInterface $storeLocatorRepository
     */
    public function __construct(
        Context $context,
        StoreLocatorRepositoryInterface $storeLocatorRepository,
    ) {

        $this->storeLocatorRepository = $storeLocatorRepository;
        parent::__construct($context);
    }

    /**
     * Deleting the car(s)
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $Ids = $data['selected'];
        $sizeIds = count($Ids);
        try {
            foreach ($Ids as $Id) {
                $this->storeLocatorRepository->deleteById($Id);
            }
            $sizeIds === 1 ? $this->messageManager->addSuccessMessage(__('Store data has been successfully deleted.')) :
                $this->messageManager->addSuccessMessage(__('Stores data has been successfully deleted.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }
        $this->_redirect('*/*/');
    }
}

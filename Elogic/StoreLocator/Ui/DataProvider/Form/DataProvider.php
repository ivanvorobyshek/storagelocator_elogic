<?php

namespace Elogic\StoreLocator\Ui\DataProvider\Form;

use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{

    /**
     * @var
     */
    protected $collection;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

    /**
     * Get Data
     *
     * @return array|mixed
     */
    public function getData()
    {
        $result = [];
        foreach ($this->collection->getItems() as $item) {
            $result[$item->getId()]['general'] = $item->getData();
        }

        //load picture for ImageUploader
        if (isset($item) && $result !== []) {

            $img = explode("|", $result[$item->getId()]['general']['image']);
            if (isset($img[0], $img[1], $img[2])) {
                $image[0]['name'] = $img[1];
                $image[0]['url'] = $img[0];
                $image[0]['type'] = "image";
                $image[0]['size'] = $img[2];
                $image[0]['file'] = $img[1];
                $result[$item->getId()]['general']['store_image'] = $image;
            }
        }
        return $result;
    }
}

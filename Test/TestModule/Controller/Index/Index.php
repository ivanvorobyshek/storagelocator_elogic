<?php

namespace Test\TestModule\Controller\Index;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Test\TestModule\MyClasses\ClassForDi;

class Index implements HttpGetActionInterface
{

    /**
     * @var PageFactory
     */
    private PageFactory $pageFactory;

    private ClassForDi $classForDi;

    public function __construct(
        PageFactory $pageFactory,
        ClassForDi $classForDi
    ) {
        $this->pageFactory = $pageFactory;
        $this->classForDi = $classForDi;
    }


    /**
     * Generate a Page
     *
     * @return ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {
        $t = $this->classForDi;
        $result = $this->pageFactory->create();
        $result->getConfig()->getTitle()->prepend('TEST');
        return $result;
    }
}

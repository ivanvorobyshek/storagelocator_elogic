<?php

namespace Test\TestModule\MyClasses;

class ClassForDi
{

    public $arg;

    public function __construct($arg) {
        $this->arg = $arg;
    }

    public function getArg(){
        return $this->arg;
    }
}
